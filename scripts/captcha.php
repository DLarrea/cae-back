<?php
include('../utils/arrestdb.php');

session_start();

$permitted_chars = 'abcdefghijkmnpqrstuvwxyz123456789';
function generate_string($input, $strength = 10)
{

    $input_length = strlen($input);

    $random_string = '';

    for ($i = 0; $i < $strength; $i++) {

        $random_character = $input[mt_rand(0, $input_length - 1)];

        $random_string .= $random_character;
    }
    return $random_string;
}
$image = imagecreatetruecolor(200, 50);
$colors = [];
$red = rand(125, 175);
$green = rand(125, 175);
$blue = rand(125, 175);
for ($i = 0; $i < 5; $i++) {
    $colors[] = imagecolorallocate($image, $red - 20 * $i, $green - 20 * $i, $blue - 20 * $i);
}
imagefill($image, 0, 0, $colors[0]);
for ($i = 0; $i < 10; $i++) {
    imagesetthickness($image, rand(2, 10));
    $line_color = $colors[rand(1, 4)];
    imagerectangle($image, rand(-10, 190), rand(-10, 10), rand(-10, 190), rand(40, 60), $line_color);
}
$white = imagecolorallocate($image, 255, 255, 255);
$textcolors = [$white];
$fonts = [dirname(__FILE__) . '/fonts/Acme.ttf', dirname(__FILE__) . '/fonts/Ubuntu.ttf', dirname(__FILE__) . '/fonts/Merriweather.ttf'];
$string_length = 6;
$captcha_string = generate_string($permitted_chars, $string_length);
$_SESSION['captcha_text'] = $captcha_string;
for ($i = 0; $i < $string_length; $i++) {
    $letter_space = 170 / $string_length;
    $initial = 15;
    imagettftext($image, 24, rand(-15, 15), $initial + $i * $letter_space, rand(25, 45), $textcolors[0], $fonts[array_rand($fonts)], $captcha_string[$i]);
}

$ip = $_SERVER["REMOTE_ADDR"];
ArrestDB::Query("DELETE FROM cae_ip WHERE ip = ?", [$ip]);
ArrestDB::Query("INSERT INTO cae_ip (ip, captcha) VALUES(?,?)", [$ip, $captcha_string]);

header('Content-type: image/png');
imagepng($image);
imagedestroy($image);