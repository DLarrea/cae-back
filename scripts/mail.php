<?php

function sendMail($mail, $subject, $message){

    $env = parse_ini_file(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .  '.env');
    $sendgridApiKey = $env['SEND_GRID_API_KEY'];
    
    $data = array(
        'personalizations' => array(
            array(
                'to' => array(
                    array('email' => $mail)
                )
            )
        ),
        'from' => array('email' => 'accounts@csa-caesystem.site'),
        'subject' => $subject,
        'content' => array(
            array(
                'type' => 'text/html',
                'value' => $message
            )
        )
    );
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api.sendgrid.com/v3/mail/send');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Bearer ' . $sendgridApiKey,
        'Content-Type: application/json'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    curl_exec($ch);
    curl_close($ch);

}
