<?php
function load($path){foreach (glob($path) as $filename){require($filename);}}

load('./utils/*.php');
load('./services/rol/*.php');
load('./services/materia/*.php');
load('./services/curso/*.php');
load('./services/estudiante/*.php');
load('./services/responsable/*.php');
load('./services/usuario/*.php');
load('./services/profesor/*.php');
load('./services/auth/*.php');
load('./scripts/mail.php');

cors();

if (strcmp(PHP_SAPI, 'cli') === 0)
{
	exit('ArrestDB should not be run from CLI.' . PHP_EOL);
}

if (array_key_exists('_method', $_GET) === true)
{
	$_SERVER['REQUEST_METHOD'] = strtoupper(trim($_GET['_method']));
}

if(!in_array($_SERVER['PATH_INFO'], ['/login', '/recuperar-cuenta'])) JWT::verificarJWT();

ArrestDB::Serve('GET', '/(#any)/(#num)?', function ($table, $id = null)
{
	global $permissions;

	if(!JWT::hasPermission($table)){
		die(ArrestDB::Reply(ArrestDB::$HTTP[403]));
	}	
	
	if($table == 'usuario-cursos'){
        return getProfesorCursosById($id);
    }

	if($table == 'usuario-materias'){
	   $curso_id = $_GET['curso'];
	   return getProfesorCursosMateriasById($id,$curso_id);
   	}

   	if($table == 'usuario-estudiantes'){
		$curso_id = $_GET['curso'];
		$materia_id = $_GET['materia'];
		$usuario_id = $_GET['usuario'];
		return getAsistenciaEstudianteById($curso_id, $materia_id, $usuario_id);
	}

	if($table == 'rol'){
        return isset($id) && is_numeric($id) ? getRolById($id) : getRoles();
    }
	
	if($table == 'curso'){
        return isset($id) && is_numeric($id) ? getCursoById($id) : getCursos();
    }
	
	if($table == 'materia'){
        return isset($id) && is_numeric($id) ? getMateriaById($id) : getMaterias();
    }
	
	if($table == 'estudiante'){
        return isset($id) && is_numeric($id) ? getEstudianteById($id) : getEstudiantes();
    }
	
	if($table == 'estudiante-asistencias'){
        return getAsistenciasByEstudianteId($id);
    }
	
	if($table == 'responsable'){
        return isset($id) && is_numeric($id) ? getResponsableById($id) : getResponsables();
    }
	
	if($table == 'profesor'){
        return isset($id) && is_numeric($id) ? getProfesorById($id) : getProfesores();
    }
	
	if($table == 'responsable-estudiantes'){
        return getEstudiantesACargo($id);
    }

	if($table == 'usuario'){
        return isset($id) && is_numeric($id) ? getUsuarioById($id) : getUsuarios();
    }

	if($table == 'curso-estudiantes'){
        return getEstudiantesByCursoId($id);
    }
	
	if($table == 'curso-materia-profesor'){
        return getMateriaProfesorByCursoId($id);
    }

	if($table == 'historial-asistencias'){
		$curso_id = $_GET['curso'];
		$materia_id = $_GET['materia'];
		$usuario_id = $_GET['usuario'];
		return getHistorialAsistencias($curso_id, $materia_id, $usuario_id);
	}

	$response = ArrestDB::$HTTP[404];
	return ArrestDB::Reply($response);
});

ArrestDB::Serve('DELETE', '/(#any)/(#num)', function ($table, $id)
{
	global $permissions;

	if(!JWT::hasPermission($table)){
		die(ArrestDB::Reply(ArrestDB::$HTTP[403]));
	}

	if($table == 'curso'){
        return deleteCurso($id);
    }
	
	if($table == 'materia'){
        return deleteMateria($id);
    }
	
	if($table == 'estudiante'){
        return deleteEstudiante($id);
    }
	
	if($table == 'responsable'){
        return deleteResponsable($id);
    }
	
	if($table == 'profesor'){
        return deleteProfesor($id);
    }

	if($table == 'curso-materia-profesor'){
        return deleteCursoMateriaProfesor($id);
    }

	$response = ArrestDB::$HTTP[404];
	return ArrestDB::Reply($response);
});

if (in_array($http = strtoupper($_SERVER['REQUEST_METHOD']), ['POST', 'PUT']) === true)
{
	if (preg_match('~^\x78[\x01\x5E\x9C\xDA]~', $data = file_get_contents('php://input')) > 0)
	{
		$data = gzuncompress($data);
	}

	if ((array_key_exists('CONTENT_TYPE', $_SERVER) === true) && (empty($data) !== true))
	{
		if (strncasecmp($_SERVER['CONTENT_TYPE'], 'application/json', 16) === 0)
		{
			$GLOBALS['_' . $http] = json_decode($data, true);
		}

		else if ((strncasecmp($_SERVER['CONTENT_TYPE'], 'application/x-www-form-urlencoded', 33) === 0) && (strncasecmp($_SERVER['REQUEST_METHOD'], 'PUT', 3) === 0))
		{
			parse_str($data, $GLOBALS['_' . $http]);
		}
	}

	if ((isset($GLOBALS['_' . $http]) !== true) || (is_array($GLOBALS['_' . $http]) !== true))
	{
		$GLOBALS['_' . $http] = [];
	}

	unset($data);
}

ArrestDB::Serve('POST', '/(#any)', function ($table)
{

	global $permissions;

	if(!JWT::hasPermission($table)){
		die(ArrestDB::Reply(ArrestDB::$HTTP[403]));
	}

	if($table == 'registrar-asistencia'){
        return registrarAsistencia($_POST);
    }
	
	if($table == 'curso'){
        return postCurso($_POST);
    }
	
	if($table == 'curso-asociar'){
        return asocialCursoMateriaProfesor($_POST);
    }
	
	if($table == 'materia'){
        return postMateria($_POST);
    }
	
	if($table == 'estudiante'){
        return postEstudiante($_POST);
    }
	
	if($table == 'responsable'){
        return postResponsable($_POST);
    }
	
	if($table == 'profesor'){
        return postProfesor($_POST);
    }
	
	if($table == 'login'){
        return login($_POST);
    }
	
	if($table == 'usuario'){
        return altaUsuario($_POST);
    }
	
	if($table == 'recuperar-cuenta'){
        return recuperarCuenta($_POST);
    }
	
	if($table == 'estudiante-justificar-ausencia'){
        return justificarAusencia($_POST);
    }

	$response = ArrestDB::$HTTP[404];
	return ArrestDB::Reply($response);
});

ArrestDB::Serve('PUT', '/(#any)/(#num)', function ($table, $id)
{

	global $permissions;

	if(!JWT::hasPermission($table)){
		die(ArrestDB::Reply(ArrestDB::$HTTP[403]));
	}

	if($table == 'curso'){
        return putCurso($id, $GLOBALS['_PUT']);
    }
	
	if($table == 'materia'){
        return putMateria($id, $GLOBALS['_PUT']);
    }
	
	if($table == 'estudiante'){
        return putEstudiante($id, $GLOBALS['_PUT']);
    }
	
	if($table == 'responsable'){
        return putResponsable($id, $GLOBALS['_PUT']);
    }
	
	if($table == 'profesor'){
        return putProfesor($id, $GLOBALS['_PUT']);
    }
	
	if($table == 'usuario'){
        return putUsuario($id, $GLOBALS['_PUT']);
    }
	
	if($table == 'usuario-estado'){
        return cambiarEstadoUsuario($id, $GLOBALS['_PUT']);
    }

	$response = ArrestDB::$HTTP[404];
	return ArrestDB::Reply($response);
});