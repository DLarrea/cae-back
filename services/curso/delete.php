<?php

function deleteCurso($id){
    
    try {
        
        ArrestDB::Query("UPDATE cae_curso_materia_profesor SET activo = 0 WHERE curso_id = ?", [$id]);
        ArrestDB::Query("UPDATE cae_curso SET activo = 0 WHERE id = ?", [$id]);
        $response = ArrestDB::$HTTP[200];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
    
}

function deleteCursoMateriaProfesor($id){
    
    try {
        
        ArrestDB::Query("UPDATE cae_curso_materia_profesor SET activo = 0 WHERE id = ?", [$id]);
        $response = ArrestDB::$HTTP[200];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
    
}