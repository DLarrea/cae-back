<?php

function getCursos(){
    try {

        $cursos = ArrestDB::Query("SELECT * FROM cae_curso WHERE activo = 1 ORDER BY fecha_actualizacion DESC");
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $cursos;
        return ArrestDB::Reply($response);

    } catch(Exception $e){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
    }
}

function getCursoById($id){
    try {

        $curso = ArrestDB::Query("SELECT * FROM cae_curso WHERE id = ? AND activo = 1 LIMIT 1", [$id]);
        $estudiantes = ArrestDB::Query("SELECT * FROM v_cae_estudiante WHERE curso_id = ? AND activo = 1", [$id]);
        $curso = array_shift($curso);
        $curso->estudiantes = $estudiantes;

        if(count($curso) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $curso;
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
}
function getEstudiantesByCursoId($id){
    try {

        $estudiantes = ArrestDB::Query("SELECT * FROM v_cae_estudiante WHERE curso_id = ? AND activo = 1", [$id]);
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $estudiantes;
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
}

function getMateriaProfesorByCursoId($id){
    try {

        $materia_profesor = ArrestDB::Query("SELECT * FROM v_cae_curso_materia_profesor WHERE curso_id = ? AND activo = 1", [$id]);
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $materia_profesor;
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
}