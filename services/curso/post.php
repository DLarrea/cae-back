<?php

function postCurso($data){

    try {

        $nombre = $data['nombre'];
        ArrestDB::Query("INSERT INTO cae_curso(nombre) VALUES(?)", [$nombre]);
        $response = ArrestDB::$HTTP[201];
        return ArrestDB::Reply($response);

    } catch(Exception $e){
        
        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
    
    }

}

function asocialCursoMateriaProfesor($data){

    try {

        $curso_id = $data['curso_id'];
        $materia_id = $data['materia_id'];
        $persona_id = $data['persona_id'];

        $cmp = ArrestDB::Query("SELECT * FROM cae_curso_materia_profesor WHERE curso_id = ? AND materia_id = ? AND persona_id = ? AND activo = 1", [$curso_id, $materia_id, $persona_id]);
        if(count($cmp) > 0){
            $response = ArrestDB::$HTTP[400];
            $response['message'] = 'Ya se encuentran asociados';
            return ArrestDB::Reply($response);
        }

        ArrestDB::Query("INSERT INTO cae_curso_materia_profesor (curso_id, materia_id, persona_id) VALUES(?,?,?)", [$curso_id, $materia_id, $persona_id]);

        $response = ArrestDB::$HTTP[201];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);

    }

}