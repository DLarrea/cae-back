<?php

function putCurso($id, $data){

    try {

        $nombre = $data['nombre'];
        ArrestDB::Query("UPDATE cae_curso SET nombre = ? WHERE id = ?", [$nombre, $id]);
        $response = ArrestDB::$HTTP[200];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
    
    }
}