<?php

function login($data){
    
    $email = $data['email'];
    $pass = $data['pass'];
    $captcha = $data['captcha'];

    if(empty($email) || empty($pass) || empty($captcha)){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = "Campos [email, pass, captcha] requeridos";
        return ArrestDB::Reply($response);
    }

    $ip = $_SERVER["REMOTE_ADDR"];
    $captcha = ArrestDB::Query("SELECT * FROM cae_ip WHERE ip = ? AND captcha = ?", [$ip, $captcha]);
    if(count($captcha) == 0){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = "Usuario y/o contraña incorrecta/s";
        return ArrestDB::Reply($response);
    }

    $usuario = ArrestDB::Query("SELECT * FROM v_cae_usuario WHERE password = ? AND email = ? AND activo = ?", [md5($pass), $email, 1]);
    if(count($usuario) == 0){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = "Usuario y/o contraña incorrecta/s";
        return ArrestDB::Reply($response);
    }
    $usuario = array_shift($usuario);

    $data = new stdClass();
    $data->id = $usuario->id;
    $data->email = $usuario->email;
    $data->nombres = $usuario->persona_nombres;
    $data->apellidos = $usuario->persona_apellidos;
    $data->documento = $usuario->persona_documento;
    $data->rol = $usuario->rol_nombre;
    $token = JWT::generarJWT($data);

    $response = ArrestDB::$HTTP[200];
    $response['result'] = $token;
    return ArrestDB::Reply($response);

}