<?php

function deleteEstudiante($id){
    
    try {
        
        ArrestDB::Query("UPDATE cae_estudiante SET activo = 0 WHERE id = ?", [$id]);
        $response = ArrestDB::$HTTP[200];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
    
}