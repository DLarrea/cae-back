<?php

function getEstudiantes(){
    try {

        $estudiantes = ArrestDB::Query("SELECT * FROM v_cae_estudiante WHERE activo = 1 ORDER BY curso_nombre ASC");
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $estudiantes;
        return ArrestDB::Reply($response);

    } catch(Exception $e){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
    }
}

function getEstudianteById($id){
    try {

        $estudiante = ArrestDB::Query("SELECT e.id, e.persona_id, e.responsable_id, e.curso_id, p.documento, p.nombres, p.apellidos, p.sexo, p.fecha_nacimiento, p.telefono, p.email, p.ciudad, p.barrio, p.direccion FROM cae_estudiante e JOIN cae_persona p on e.persona_id = p.id  WHERE e.id = ?", [$id]);
        if(count($estudiante) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $response = ArrestDB::$HTTP[200];
        $response['result'] = array_shift($estudiante);
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
}
function getAsistenciaEstudianteById($curso_id, $materia_id, $usuario_id){
    try {

        $estudiantes = ArrestDB::Query("SELECT p.*, e.id as estudiante_id FROM cae_estudiante e JOIN cae_persona p ON e.persona_id = p.id  WHERE e.curso_id = ? AND e.activo = ?",[$curso_id, 1]);
        $curso = ArrestDB::Query("SELECT * FROM cae_curso WHERE id = ?", [$curso_id]);
        $materia = ArrestDB::Query("SELECT * FROM cae_materia WHERE id = ?", [$materia_id]);
        $persona = ArrestDB::Query("SELECT * FROM cae_persona WHERE id = (SELECT persona_id FROM cae_usuario WHERE id = ? LIMIT 1)", [$usuario_id]);

        if(count($curso) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $curso = array_shift($curso);
        
        if(count($materia) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $materia = array_shift($materia);
        
        if(count($persona) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $persona = array_shift($persona);

        $result = new stdClass();
        $result->estudiantes = $estudiantes;
        $result->materia = $materia;
        $result->curso = $curso;
        $result->profesor = $persona;

        $response = ArrestDB::$HTTP[200];
        $response['result'] = $result;
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
}

function getAsistenciasByEstudianteId($id){
    try {

        $asistencias = ArrestDB::Query("SELECT * FROM v_cae_asistencia WHERE estudiante_id = ? ORDER BY fecha_actualizacion DESC", [$id]);
        $estudiante = ArrestDB::Query("SELECT p.*, e.id as estudiante_id FROM cae_estudiante e JOIN cae_persona p ON e.persona_id = p.id  WHERE e.id = ? AND e.activo = ?",[$id, 1]);
        
        $result = new stdClass();
        $result->asistencias = $asistencias;
        $result->estudiante = array_shift($estudiante);

        $response = ArrestDB::$HTTP[200];
        $response['result'] = $result;
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
}