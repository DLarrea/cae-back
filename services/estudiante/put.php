<?php

function putEstudiante($id, $data){

    try {

        $estudiante = ArrestDB::Query("SELECT * FROM cae_estudiante WHERE id = ?", [$id]);
        if(count($estudiante) == 0){
            $response = ArrestDB::$HTTP[404];
            $response['message'] = 'El registro no existe';
            return ArrestDB::Reply($response);
        }
        $estudiante = array_shift($estudiante);

        putPersona($estudiante->persona_id, $data);

        //Curso
        $curso_id = $data['curso_id'];
        $curso = ArrestDB::Query("SELECT * FROM cae_curso WHERE id = ?", [$curso_id]);
        if(count($curso) > 0){
            $curso = array_shift($curso);
        }
        
        //Responsable
        $responsable_id = $data['responsable_id'];
        $responsable = ArrestDB::Query("SELECT * FROM cae_persona WHERE id = ?", [$responsable_id]);
        if(count($responsable) > 0){
            $responsable = array_shift($responsable);
        }

        if($curso_id == $estudiante->curso_id){
            ArrestDB::Query("UPDATE cae_estudiante SET curso_id = ?, responsable_id = ? WHERE id = ?", [$curso->id, $responsable->id, $id]);
        } else {
            ArrestDB::Query("UPDATE cae_estudiante SET activo = 0 WHERE id = ?", [$id]);
            ArrestDB::Query("INSERT INTO cae_estudiante(persona_id,curso_id,responsable_id) VALUES(?,?,?)", [$estudiante->persona_id, $curso->id, $responsable->id]);
        }


        $response = ArrestDB::$HTTP[200];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);

    }

}