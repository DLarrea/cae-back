<?php

function postEstudiante($data){

    try {

        $persona = postPersona($data);

        //Curso
        $curso_id = $data['curso_id'];
        $curso = ArrestDB::Query("SELECT * FROM cae_curso WHERE id = ?", [$curso_id]);
        if(count($curso) > 0){
            $curso = array_shift($curso);
        }
        
        //Responsable
        $responsable_id = $data['responsable_id'];
        $responsable = ArrestDB::Query("SELECT * FROM cae_persona WHERE id = ?", [$responsable_id]);
        if(count($responsable) > 0){
            $responsable = array_shift($responsable);
        }

        ArrestDB::Query("INSERT INTO cae_estudiante(persona_id,curso_id,responsable_id) VALUES(?,?,?)", [$persona->id, $curso->id, $responsable->id]);

        $response = ArrestDB::$HTTP[201];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);

    }

}

function registrarAsistencia($data){

    $asistencias = $data['asistencias'];
    $nota = $data['nota'];
    $curso_id = $data['curso'];
    $materia_id = $data['materia'];

    $cmp = ArrestDB::Query("SELECT * FROM cae_curso_materia_profesor WHERE curso_id = ? AND materia_id = ? AND activo = 1", [$curso_id, $materia_id]);

    if(count($cmp) == 0){
        $response = ArrestDB::$HTTP[404];
        return ArrestDB::Reply($response);
    }
    $cmp = array_shift($cmp);

    if(!is_array($asistencias)){
        $response = ArrestDB::$HTTP[400];
        return ArrestDB::Reply($response);
    }

    $fecha_actual = date("Y-m-d");
    foreach($asistencias as $as){
        $asistencia = $as['asistencia'] == 1 ? 1 : 0;
        $estudiante_id = $as['estudiante_id'];
        ArrestDB::Query("INSERT INTO cae_asistencia (estudiante_id, curso_materia_profesor_id, asistencia, fecha) VALUES(?,?,?,?)", [$estudiante_id, $cmp->id, $asistencia, $fecha_actual]);
    }


    if(!empty($nota)){
        ArrestDB::Query("INSERT INTO cae_asistencia_nota (nota, curso_materia_profesor_id) VALUES(?,?)", [$nota, $cmp->id]);
    }

    $response = ArrestDB::$HTTP[201];
    return ArrestDB::Reply($response);

}

function justificarAusencia($data){

    try {

        $id = $data['id'];
        $asistencia_justificacion = $data['asistencia_justificacion'];

        ArrestDB::Query("UPDATE cae_asistencia SET asistencia_justificacion = ? WHERE id = ?", [$asistencia_justificacion, $id]);

        $response = ArrestDB::$HTTP[201];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);

    }

}