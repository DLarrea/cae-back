<?php

function putProfesor($id, $data){

    try {

        $email = $data['email'];
        
        $persona = ArrestDB::Query("SELECT * FROM cae_persona WHERE email = ?", [$email]);
        if(count($persona) > 0){
            $persona = array_shift($persona);
            if($persona->id != $data['id']){
                $response = ArrestDB::$HTTP[400];
                $response['message'] = "El email $email ya se encuentra en uso";
                return ArrestDB::Reply($response);
            }
        }
        putPersona($id, $data);

        $response = ArrestDB::$HTTP[200];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);

    }

}