<?php

function getProfesores(){
    try {

        $profesores = getPersonas("P");
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $profesores;
        return ArrestDB::Reply($response);

    } catch(Exception $e){
        $response = ArrestDB::$HTTP[500];
        return ArrestDB::Reply($response);
    }
}

function getProfesorById($id){
    try {

        $profesor = getPersonaById($id);
        
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $profesor;
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
}

function getProfesorCursosById($id){
    try {
        $usuario = ArrestDB::Query("SELECT * FROM v_cae_usuario WHERE id = ? LIMIT 1", [$id]);
        if(count($usuario) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }

        $usuario = array_shift($usuario);
        $cursos = ArrestDB::Query("SELECT DISTINCT curso_id FROM cae_curso_materia_profesor WHERE persona_id = ? AND activo = ?", [$usuario -> persona_id,1]);
        
        if(count($cursos) > 0){
            $marcadores.= implode(',', array_fill(0, count($cursos), '?'));
            foreach ($cursos as $curso) {
                $cursoIds[] = $curso->curso_id;
            }
            $cursos = ArrestDb::Query( "SELECT * FROM cae_curso WHERE id IN ($marcadores) AND activo = 1",$cursoIds);
        }
        
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $cursos;
        return ArrestDB::Reply($response);

    } catch(Exception $e){
        $response = ArrestDB::$HTTP[500];
        return ArrestDB::Reply($response);
    }  
}
function getProfesorCursosMateriasById($id,$curso_id){
    try {
        $usuario = ArrestDB::Query("SELECT * FROM v_cae_usuario WHERE id = ? LIMIT 1", [$id]);
        if(count($usuario) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $usuario = array_shift($usuario);
        
        $materias = ArrestDB::Query("SELECT DISTINCT materia_id FROM cae_curso_materia_profesor WHERE persona_id = ? AND curso_id = ? AND activo = ?",[$usuario -> persona_id,$curso_id,1]);
    
        if(count($materias) > 0){
            $marcadores.= implode(',', array_fill(0, count($materias), '?'));
            foreach ($materias as $materia) {
                $marteriasIds[] = $materia->materia_id;
            }
            $materias = ArrestDb::Query("SELECT nombre,id FROM cae_materia WHERE id IN ($marcadores) AND activo = 1",$marteriasIds);
        }
        $curso = ArrestDb::Query("SELECT nombre,id FROM cae_curso WHERE id = ? AND activo = 1",[$curso_id]);

        $result = new stdClass();
        $result->curso = array_shift($curso);
        $result->materias = $materias;
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $result;
        return ArrestDB::Reply($response);
    
    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
    }
}