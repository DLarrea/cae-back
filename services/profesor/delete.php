<?php

function deleteProfesor($id){
    
    try {
        
        ArrestDB::Query("UPDATE cae_curso_materia_profesor SET activo = 0 WHERE persona_id = ?", [$id]);
        ArrestDB::Query("UPDATE cae_persona SET activo = 0 WHERE id = ?", [$id]);
        ArrestDB::Query("UPDATE cae_usuario SET activo = 0 WHERE persona_id = ?", [$id]);
        $response = ArrestDB::$HTTP[200];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
    
}