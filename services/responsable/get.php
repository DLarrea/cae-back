<?php

function getResponsables(){
    try {

        $responsables = getPersonas("R");
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $responsables;
        return ArrestDB::Reply($response);

    } catch(Exception $e){
        $response = ArrestDB::$HTTP[500];
        return ArrestDB::Reply($response);
    }
}

function getResponsableById($id){
    try {

        $responsable = getPersonaById($id);
        
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $responsable;
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
}

function getEstudiantesACargo($id){
    try {

        $estudiantes = ArrestDB::Query("SELECT * FROM v_cae_estudiante WHERE responsable_id = (SELECT persona_id FROM cae_usuario WHERE id = ? LIMIT 1) AND activo = 1", [$id]);
        
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $estudiantes;
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
}