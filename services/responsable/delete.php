<?php

function deleteResponsable($id){
    
    try {
        
        $estudiante = ArrestDB::Query("SELECT * FROM cae_estudiante WHERE responsable_id = ?", [$id]);
        if(count($estudiante) > 0){
            $response = ArrestDB::$HTTP[400];
            $response['message'] = 'El responsable se encuentra asociado a uno o más estudiantes';
            return ArrestDB::Reply($response);
        }

        ArrestDB::Query("UPDATE cae_persona SET activo = 0 WHERE id = ?", [$id]);
        ArrestDB::Query("UPDATE cae_usuario SET activo = 0 WHERE persona_id = ?", [$id]);
        $response = ArrestDB::$HTTP[200];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
    
}