<?php

function postResponsable($data){

    try {

        $email = $data['email'];
        
        $persona = ArrestDB::Query("SELECT * FROM cae_persona WHERE email = ?", [$email]);
        if(count($persona) > 0){
            $response = ArrestDB::$HTTP[400];
            $response['message'] = "El email $email ya se encuentra en uso";
            return ArrestDB::Reply($response);
        }

        $persona = postPersona($data);

        postUsuario(array('email' => $email, 'persona_id' => $persona->id, 'rol' => 'RESPONSABLE'));

        $response = ArrestDB::$HTTP[201];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);

    }

}