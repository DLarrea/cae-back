<?php

function getMaterias(){
    try {

        $materias = ArrestDB::Query("SELECT id,nombre,fecha_actualizacion FROM cae_materia ORDER BY fecha_actualizacion DESC");
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $materias;
        return ArrestDB::Reply($response);

    } catch(Exception $e){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
    }
}

function getMateriaById($id){
    try {

        $materia = ArrestDB::Query("SELECT id,nombre,fecha_actualizacion FROM cae_materia WHERE id = ? LIMIT 1", [$id]);
        if(count($materia) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $response = ArrestDB::$HTTP[200];
        $response['result'] = array_shift($materia);
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
}

function getHistorialAsistencias($curso_id, $materia_id, $usuario_id){

    try {
    
        if(empty($curso_id) || empty($materia_id) || empty($usuario_id)){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }

        $cmp = ArrestDB::Query("SELECT * FROM cae_curso_materia_profesor WHERE curso_id = ? AND materia_id = ? AND activo = 1", [$curso_id, $materia_id]);

        if(count($cmp) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $cmp = array_shift($cmp);

        $curso = ArrestDB::Query("SELECT * FROM cae_curso WHERE id = ?", [$curso_id]);
        if(count($curso) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $curso = array_shift($curso);
        
        $materia = ArrestDB::Query("SELECT * FROM cae_materia WHERE id = ?", [$materia_id]);
        if(count($materia) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $materia = array_shift($materia);

        $profesor = ArrestDB::Query("SELECT * FROM cae_persona WHERE id = (SELECT persona_id FROM cae_usuario WHERE id = ? LIMIT 1)", [$usuario_id]);
        if(count($profesor) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $profesor = array_shift($profesor);

        $asistencias = ArrestDB::Query("SELECT * FROM v_cae_asistencia WHERE curso_materia_profesor_id = ? ORDER BY fecha DESC", [$cmp->id]);
        $notas = ArrestDB::Query("SELECT * FROM cae_asistencia_nota WHERE curso_materia_profesor_id = ? ORDER BY fecha_actualizacion DESC", [$cmp->id]);

        $result = new stdClass();
        $result->asistencias = $asistencias;
        $result->materia = $materia;
        $result->curso = $curso;
        $result->profesor = $profesor;
        $result->notas = $notas;

        $response = ArrestDB::$HTTP[200];
        $response['result'] = $result;
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }

}