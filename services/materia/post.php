<?php

function postMateria($data){

    try {

        $nombre = $data['nombre'];
        ArrestDB::Query("INSERT INTO cae_materia(nombre) VALUES(?)", [$nombre]);
        $response = ArrestDB::$HTTP[201];
        return ArrestDB::Reply($response);

    } catch(Exception $e){
        
        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
    
    }

}