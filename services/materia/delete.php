<?php

function deleteMateria($id){
    
    try {
        
        ArrestDB::Query("DELETE FROM cae_materia WHERE id = ?", [$id]);
        $response = ArrestDB::$HTTP[200];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
    
}