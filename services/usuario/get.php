<?php

function getUsuarios(){
    try {

        $usuarios = ArrestDB::Query("SELECT * FROM v_cae_usuario");
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $usuarios;
        return ArrestDB::Reply($response);

    } catch(Exception $e){
        $response = ArrestDB::$HTTP[500];
        return ArrestDB::Reply($response);
    }
}

function getUsuarioById($id){
    try {

        $usuario = ArrestDB::Query("SELECT * FROM v_cae_usuario WHERE id = ? LIMIT 1", [$id]);
        if(count($usuario) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $usuario = array_shift($usuario);
        $persona = getPersonaById($usuario->persona_id);

        $response = ArrestDB::$HTTP[200];
        $response['result'] = $persona;
        return ArrestDB::Reply($response);

    } catch(Exception $e){
        $response = ArrestDB::$HTTP[500];
        return ArrestDB::Reply($response);
    }
}