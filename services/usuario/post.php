<?php

function postUsuario($data){

    $email = $data['email'];
    $persona_id = $data['persona_id'];
    $rol_asignar = $data['rol'];

    $rol_id = null;
    $rol = ArrestDB::Query("SELECT * FROM cae_rol WHERE nombre = ?", [$rol_asignar]);
    $rol = array_shift($rol);
    if($rol) $rol_id = $rol->id;
    $pw = generarPass();

    ArrestDB::Query("INSERT INTO cae_usuario (email, rol_id, persona_id, password, activo) VALUES(?,?,?,?,?)",
        [$email, $rol_id, $persona_id, md5($pw), 1]);

    $message = "<p>¡Hola! Tu contraseña para CAE System es <b>$pw</b></p>";
    sendMail($email, "Activación de cuenta - CAE System", $message);

}

function altaUsuario($data){

    $email = $data['email'];
    $rol = $data['rol'];
        
    $persona = ArrestDB::Query("SELECT * FROM cae_persona WHERE email = ?", [$email]);
    if(count($persona) > 0){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = "El email $email ya se encuentra en uso";
        return ArrestDB::Reply($response);
    }

    $persona = postPersona($data);

    postUsuario(array('email' => $email, 'persona_id' => $persona->id, 'rol' => $rol));

    $response = ArrestDB::$HTTP[201];
    return ArrestDB::Reply($response);

}

function recuperarCuenta($data){
    
    $email = $data['email'];
    $documento = $data['documento'];
    $captcha = $data['captcha'];

    $ip = $_SERVER["REMOTE_ADDR"];
    $captcha = ArrestDB::Query("SELECT * FROM cae_ip WHERE ip = ? AND captcha = ?", [$ip, $captcha]);
    if(count($captcha) == 0){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = "No se pudo procesar la solicitud1";
        return ArrestDB::Reply($response);
    }

    if(empty($email) || empty($captcha) || empty($documento)){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = "No se pudo procesar la solicitud2";
        return ArrestDB::Reply($response);
    }

    $persona = ArrestDB::Query("SELECT * FROM cae_persona WHERE documento = ?", [$documento]);
    if(count($persona) == 0){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = "No se pudo procesar la solicitud3";
        return ArrestDB::Reply($response);
    }
    $persona = array_shift($persona);

    $usuario = ArrestDB::Query("SELECT * FROM cae_usuario WHERE email = ? AND persona_id = ?", [$email, $persona->id]);
    if(count($usuario) == 0){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = "No se pudo procesar la solicitud4";
        return ArrestDB::Reply($response);
    }
    $usuario = array_shift($usuario);

    $pw = generarPass();
    ArrestDB::Query("UPDATE cae_usuario SET password = ?, activo = 1 WHERE id = ?", [md5($pw), $usuario->id]);

    $message = "<p>¡Hola! Tu contraseña para CAE System es <b>$pw</b></p>";
    sendMail($email, "Recuperación de cuenta - CAE System", $message);

    $response = ArrestDB::$HTTP[201];
    $response['message'] = "Se ha enviado una nueva contraseña a tu email";
    return ArrestDB::Reply($response);

}