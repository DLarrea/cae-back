<?php

function cambiarEstadoUsuario($id, $data){
    
    try {

        $activo = $data['activo'] == 1 ? 1 : 0;
        
        ArrestDB::Query("UPDATE cae_usuario SET activo = ? WHERE id = ?", [$activo, $id]);
        ArrestDB::Query("UPDATE cae_persona SET activo = ? WHERE id = ( SELECT persona_id FROM cae_usuario WHERE id = ? )", [$activo, $id]);

        $response = ArrestDB::$HTTP[200];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
        
    }
    
}

function putUsuario($id, $data){

    try {

        $email = $data['email'];
        
        $persona = ArrestDB::Query("SELECT * FROM cae_persona WHERE email = ?", [$email]);
        if(count($persona) > 0){
            $persona = array_shift($persona);
            if($persona->id != $data['id']){
                $response = ArrestDB::$HTTP[400];
                $response['message'] = "El email $email ya se encuentra en uso";
                return ArrestDB::Reply($response);
            }
        }
        putPersona($id, $data);

        $response = ArrestDB::$HTTP[200];
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);

    }

}