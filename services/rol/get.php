<?php

function getRoles(){
    try {

        $roles = ArrestDB::Query("SELECT id,nombre,fecha_actualizacion FROM cae_rol");
        $response = ArrestDB::$HTTP[200];
        $response['result'] = $roles;
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);
    
    }
}

function getRolById($id){
    try {

        $rol = ArrestDB::Query("SELECT id,nombre,fecha_actualizacion FROM cae_rol WHERE id = ? LIMIT 1", [$id]);
        if(count($rol) == 0){
            $response = ArrestDB::$HTTP[404];
            return ArrestDB::Reply($response);
        }
        $response = ArrestDB::$HTTP[200];
        $response['result'] = array_shift($rol);
        return ArrestDB::Reply($response);

    } catch(Exception $e){

        $response = ArrestDB::$HTTP[400];
        $response['message'] = $e->getMessage();
        return ArrestDB::Reply($response);

    }
}