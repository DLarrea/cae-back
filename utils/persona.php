<?php

function postPersona($data){
    
    $documento = $data['documento'];
    $nombres = $data['nombres'];
    $apellidos = $data['apellidos'];
    $sexo = $data['sexo'];
    $fecha_nacimiento = $data['fecha_nacimiento'];
    $ciudad = $data['ciudad'];
    $barrio = $data['barrio'];
    $direccion = $data['direccion'];
    $telefono = $data['telefono'];
    $email = $data['email'];
    $tipo = $data['tipo'];

    $persona = ArrestDB::Query("SELECT * FROM cae_persona WHERE documento = ?", [$documento]);
    if(count($persona) > 0){
        $response = ArrestDB::$HTTP[400];
        $response['message'] = 'El nro. de doc. ya existe';
        return ArrestDB::Reply($response);
    }

    ArrestDB::Query("INSERT INTO cae_persona (documento,nombres,apellidos,sexo,fecha_nacimiento,ciudad,barrio,direccion,telefono,email,tipo) VALUES(?,?,?,?,?,?,?,?,?,?,?)",
    [$documento, $nombres, $apellidos, $sexo, $fecha_nacimiento, $ciudad, $barrio, $direccion, $telefono, $email, $tipo]);

    $persona = ArrestDB::Query("SELECT * FROM cae_persona WHERE documento = ?", [$documento]);
    if(count($persona) == 0){
        throw new Exception('Error al crear persona');
    }
    $persona = array_shift($persona);
    return $persona;

}

function putPersona($id, $data){

    $nombres = $data['nombres'];
    $apellidos = $data['apellidos'];
    $sexo = $data['sexo'];
    $fecha_nacimiento = $data['fecha_nacimiento'];
    $ciudad = $data['ciudad'];
    $barrio = $data['barrio'];
    $direccion = $data['direccion'];
    $telefono = $data['telefono'];
    $email = $data['email'];
    $tipo = $data['tipo'];

    ArrestDB::Query("UPDATE cae_persona SET nombres = ?, apellidos = ?, sexo = ?, fecha_nacimiento = ?, ciudad = ?, barrio = ?, direccion = ?, telefono = ?, email = ?, tipo = ? WHERE id = ?",
    [$nombres, $apellidos, $sexo, $fecha_nacimiento, $ciudad, $barrio, $direccion, $telefono, $email, $tipo, $id]);

    $persona = ArrestDB::Query("SELECT * FROM cae_persona WHERE id = ?", [$id]);
    $persona = array_shift($persona);
    return $persona;

}

function getPersonas($tipo = null){
    if($tipo == null){
        $personas = ArrestDB::Query("SELECT * FROM cae_persona WHERE activo = 1 ORDER BY fecha_actualizacion DESC");
    } else {
        $personas = ArrestDB::Query("SELECT * FROM cae_persona WHERE tipo IN (?) AND activo = 1 ORDER BY fecha_actualizacion DESC", [$tipo]);
    }
    return $personas;
}

function getPersonaById($id){
    $persona = ArrestDB::Query("SELECT * FROM cae_persona WHERE id = ? AND activo = 1", [$id]);
    $persona = array_shift($persona);
    return $persona;
}