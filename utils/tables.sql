CREATE TABLE cae_rol (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(510) UNIQUE NOT NULL,
	fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

INSERT INTO cae_rol (nombre) values ('ADMIN'), ('PROFESOR'), ('RESPONSABLE');

CREATE TABLE cae_curso (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(510) UNIQUE NOT NULL,
	activo TINYINT DEFAULT 1,
	fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE cae_materia (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(510) UNIQUE NOT NULL,
	activo TINYINT DEFAULT 1,
	fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE cae_persona (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	documento VARCHAR(510) UNIQUE NOT NULL,
	nombres VARCHAR(510) NOT NULL,
	apellidos VARCHAR(510) NOT NULL,
	sexo ENUM('F', 'M') DEFAULT 'M',
    fecha_nacimiento DATE DEFAULT NULL,
	ciudad VARCHAR(510) DEFAULT NULL,
	barrio VARCHAR(510) DEFAULT NULL,
	direccion VARCHAR(510) DEFAULT NULL,
	telefono VARCHAR(510) DEFAULT NULL,
	email VARCHAR(510) DEFAULT NULL,
	tipo ENUM('P', 'E', 'R', 'ADMIN') DEFAULT 'E',
	activo TINYINT DEFAULT 1,
	fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE cae_curso_materia_profesor (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	curso_id INTEGER NOT NULL,
	materia_id INTEGER NOT NULL,
	persona_id INTEGER NOT NULL,
	fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	activo TINYINT DEFAULT 1,
	FOREIGN KEY(curso_id) REFERENCES cae_curso(id),
	FOREIGN KEY(materia_id) REFERENCES cae_materia(id),
	FOREIGN KEY(persona_id) REFERENCES cae_persona(id)
);

CREATE TABLE cae_estudiante (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	persona_id INTEGER NOT NULL,
	responsable_id INTEGER,
	curso_id INTEGER,
	activo TINYINT DEFAULT 1,
	fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	FOREIGN KEY(persona_id) REFERENCES cae_persona(id),
	FOREIGN KEY(responsable_id) REFERENCES cae_persona(id),
	FOREIGN KEY(curso_id) REFERENCES cae_curso(id)
);

CREATE TABLE cae_asistencia (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	estudiante_id INTEGER NOT NULL,
	curso_materia_profesor_id INTEGER,
	asistencia TINYINT DEFAULT 1,
	fecha DATE DEFAULT NULL,
	asistencia_justificacion TEXT DEFAULT NULL,
	fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	FOREIGN KEY(estudiante_id) REFERENCES cae_estudiante(id),
	FOREIGN KEY(curso_materia_profesor_id) REFERENCES cae_curso_materia_profesor(id)
);

CREATE TABLE cae_asistencia_nota (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	curso_materia_profesor_id INTEGER,
	nota TEXT DEFAULT NULL,
	fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	FOREIGN KEY(curso_materia_profesor_id) REFERENCES cae_curso_materia_profesor(id)
);

CREATE TABLE cae_usuario (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	email VARCHAR(510) UNIQUE NOT NULL,
	password VARCHAR(510) NOT NULL,
    activo TINYINT DEFAULT 1,
    rol_id INTEGER,
    persona_id INTEGER,
	ultimo_inicio_sesion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY(rol_id) REFERENCES cae_rol(id),
    FOREIGN KEY(persona_id) REFERENCES cae_persona(id)
);

INSERT INTO cae_persona(nombres, apellidos, documento, email) VALUES ('Admin', 'CAE System', 0, 'admin@csa-caesystem.site');
INSERT INTO cae_usuario(email, password, activo, rol_id, persona_id) VALUES ('admin@csa-caesystem.site', md5('Admin.2023'), 1, (SELECT id FROM cae_rol WHERE nombre = 'ADMIN' LIMIT 1),(SELECT id FROM cae_persona WHERE documento = 0 LIMIT 1));

CREATE TABLE cae_ip (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	ip VARCHAR(255) NOT NULL,
	captcha VARCHAR(6) NOT NULL
);

CREATE OR REPLACE VIEW v_cae_estudiante
AS SELECT 
	e.id,
	p.documento as estudiante_documento,
	p.nombres as estudiante_nombres,
	p.apellidos as estudiante_apellidos,
	e.activo,
	e.fecha_actualizacion as estudiante_fecha_actualizacion,
	p.fecha_actualizacion as persona_fecha_actualizacion,
	r.id as responsable_id,
	r.documento as responsable_documento,
	r.nombres as responsable_nombres,
	r.apellidos as responsable_apellidos,
	c.nombre as curso_nombre,
	c.id as curso_id
FROM cae_estudiante e
JOIN cae_persona p on e.persona_id = p.id
LEFT JOIN cae_persona r on e.responsable_id = r.id
LEFT JOIN cae_curso c on e.curso_id = c.id; 

CREATE OR REPLACE VIEW v_cae_usuario
AS SELECT 
	u.id,
	u.password,
	u.email,
	u.rol_id,
	u.persona_id,
	u.activo,
	u.fecha_actualizacion,
	p.documento as persona_documento,
	p.nombres as persona_nombres,
	p.apellidos as persona_apellidos,
	r.nombre as rol_nombre
FROM cae_usuario u
LEFT JOIN cae_persona p on u.persona_id = p.id
LEFT JOIN cae_rol r on u.rol_id = r.id;

CREATE OR REPLACE VIEW v_cae_curso_materia_profesor
AS SELECT 
	c.*,
	p.documento as persona_documento,
	p.nombres as persona_nombres,
	p.apellidos as persona_apellidos,
	m.nombre as materia_nombre
FROM cae_curso_materia_profesor as c
LEFT JOIN cae_persona p on c.persona_id = p.id
LEFT JOIN cae_materia m on c.materia_id = m.id;

CREATE OR REPLACE VIEW v_cae_asistencia
AS SELECT 
	a.*,
	p.documento as estudiante_documento,
	p.nombres as estudiante_nombres,
	p.apellidos as estudiante_apellidos,
	e.activo,
	e.fecha_actualizacion as estudiante_fecha_actualizacion,
	p.fecha_actualizacion as persona_fecha_actualizacion,
	r.id as responsable_id,
	r.documento as responsable_documento,
	r.nombres as responsable_nombres,
	r.apellidos as responsable_apellidos,
	c.nombre as curso_nombre,
	c.id as curso_id,
	m.id as materia_id,
	m.nombre as materia_nombre
FROM cae_asistencia a
JOIN cae_estudiante e on a.estudiante_id = e.id
JOIN cae_persona p on e.persona_id = p.id
JOIN cae_curso_materia_profesor cmp on a.curso_materia_profesor_id = cmp.id
JOIN cae_curso c on e.curso_id = c.id
JOIN cae_materia m on cmp.materia_id = m.id
LEFT JOIN cae_persona r on e.responsable_id = r.id;