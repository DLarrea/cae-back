<?php

class ArrestDB
{
    private static $dbh = null;
	public static $HTTP = [
		200 => [
            'result' => null,
			'message' => null,
			'http' => [
				'code' => 200,
				'status' => 'OK',
			],
		],
		201 => [
            'result' => null,
			'message' => null,
			'http' => [
				'code' => 201,
				'status' => 'Created',
			],
		],
		204 => [
            'result' => null,
			'message' => null,
			'http' => [
				'code' => 204,
				'status' => 'No Content',
			],
		],
		400 => [
            'result' => null,
			'message' => null,
			'http' => [
				'code' => 400,
				'status' => 'Bad Request',
			],
		],
		401 => [
            'result' => null,
			'message' => null,
			'http' => [
				'code' => 401,
				'status' => 'Unauthorized',
			],
		],
		403 => [
            'result' => null,
			'message' => null,
			'http' => [
				'code' => 403,
				'status' => 'Forbidden',
			],
		],
		404 => [
            'result' => null,
			'message' => null,
			'http' => [
				'code' => 404,
				'status' => 'Not Found',
			],
		],
		409 => [
            'result' => null,
			'message' => null,
			'http' => [
				'code' => 409,
				'status' => 'Conflict',
			],
		],
		503 => [
            'result' => null,
			'message' => null,
			'http' => [
				'code' => 503,
				'status' => 'Service Unavailable',
			],
		],
	];

    static function Query($query, $values = array(), $returnValues = true){
        
		if(ArrestDB::$dbh == null){
			$data = ArrestDB::GetConnectionData();
			ArrestDB::$dbh = new PDO("mysql:host=$data->DB_HOST;dbname=$data->DB;port=$data->DB_PORT", $data->DB_USER, $data->DB_PW);
		}

		$stmt = ArrestDB::$dbh->prepare($query);
		for($i = 0; $i < count($values); $i++){
			$stmt->bindParam($i + 1, $values[$i], ArrestDB::GetParamType($values[$i]));
		}

		if(!$stmt->execute()){
			var_dump($stmt->errorInfo());
			throw new Exception("La acción no puede ser realizada");
		}

		if($returnValues){
			return $stmt->fetchAll(PDO::FETCH_OBJ);
		}
    
    }

	public static function Reply($data)
	{
		$bitmask = 0;
		$options = ['UNESCAPED_SLASHES', 'UNESCAPED_UNICODE'];

		if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) === true)
		{
			$options[] = 'PRETTY_PRINT';
		}

		foreach ($options as $option)
		{
			$bitmask |= (defined('JSON_' . $option) === true) ? constant('JSON_' . $option) : 0;
		}

		if (($result = json_encode($data, $bitmask)) !== false)
		{
			$callback = null;

			if (array_key_exists('callback', $_GET) === true)
			{
				$callback = trim(preg_replace('~[^[:alnum:]\[\]_.]~', '', $_GET['callback']));

				if (empty($callback) !== true)
				{
					$result = sprintf('%s(%s);', $callback, $result);
				}
			}

			if (headers_sent() !== true)
			{
				header(sprintf('Content-Type: application/%s; charset=utf-8', (empty($callback) === true) ? 'json' : 'javascript'));
			}
		}

        http_response_code($data['http']['code']);

		return $result;
	}

	public static function Serve($on = null, $route = null, $callback = null)
	{
		static $root = null;

		if (isset($_SERVER['REQUEST_METHOD']) !== true)
		{
			$_SERVER['REQUEST_METHOD'] = 'CLI';
		}

		if ((empty($on) === true) || (strcasecmp($_SERVER['REQUEST_METHOD'], $on) === 0))
		{
			if (is_null($root) === true)
			{
				$root = preg_replace('~/++~', '/', substr($_SERVER['PHP_SELF'], strlen($_SERVER['SCRIPT_NAME'])) . '/');
			}

			if (preg_match('~^' . str_replace(['#any', '#num'], ['[^/]++', '[0-9]++'], $route) . '~i', $root, $parts) > 0)
			{
				return (empty($callback) === true) ? true : exit(call_user_func_array($callback, array_slice($parts, 1)));
			}
		}

		return false;
	}

    public static function GetConnectionData(){
        $env = parse_ini_file(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .  '.env');
        $data = new stdClass();
        $data->DB_USER = $env['DB_USER'];
        $data->DB_PW = $env['DB_PW'];
        $data->DB = $env['DB'];
        $data->DB_PORT = $env['DB_PORT'];
        $data->DB_HOST = $env['DB_HOST'];
        return $data;
    }

	static function GetParamType($param){
        if(is_int($param)) return PDO::PARAM_INT;
        if(is_bool($param)) return PDO::PARAM_BOOL;
        return PDO::PARAM_STR;
    }

}