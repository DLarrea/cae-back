<?php

function generarPass($longitud = 10) {
    $caracteres = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $contrasena = '';
    
    for ($i = 0; $i < $longitud; $i++) {
        $indiceAleatorio = rand(0, strlen($caracteres) - 1);
        $contrasena .= $caracteres[$indiceAleatorio];
    }
    
    return $contrasena;
}