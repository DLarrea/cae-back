<?php

class JWT {

    private static $key = 'r$P9803*0bUQ$E@FvBsMEgv%';
    
    private static $permissions = array(
        "PUBLIC" => array("login", "recuperar-cuenta"),
        "RESPONSABLE" => array("responsable-estudiantes", "estudiante-asistencias", "estudiante-justificar-ausencia"),
        "PROFESOR" => array("usuario-cursos","usuario-materias","usuario-estudiantes","registrar-asistencia", "historial-asistencias"),
    );

    private static function getDataJWT($jwt){
        $tokenParts = explode('.', $jwt);
        $header = base64_decode($tokenParts[0]);
        $payload = base64_decode($tokenParts[1]);
        $signature_provided = $tokenParts[2];
    
        $expiration = json_decode($payload)->exp;
        $is_token_expired = ($expiration - time()) < 0;
    
        $base64_url_header = JWT::base64UrlEncode($header);
        $base64_url_payload = JWT::base64UrlEncode($payload);
        $signature = hash_hmac('SHA256', $base64_url_header . "." . $base64_url_payload, JWT::$key, true);
        $base64_url_signature = JWT::base64UrlEncode($signature);
    
        $is_signature_valid = ($base64_url_signature === $signature_provided);
        
        if ($is_token_expired || !$is_signature_valid) {
            return null;
        } else {
            return json_decode($payload);
        }
    }
    
    private static function validarJWT($jwt){
        $tokenParts = explode('.', $jwt);
        $header = base64_decode($tokenParts[0]);
        $payload = base64_decode($tokenParts[1]);
        $signature_provided = $tokenParts[2];
    
        $expiration = json_decode($payload)->exp;
        $is_token_expired = ($expiration - time()) < 0;
    
        $base64_url_header = JWT::base64UrlEncode($header);
        $base64_url_payload = JWT::base64UrlEncode($payload);
        $signature = hash_hmac('SHA256', $base64_url_header . "." . $base64_url_payload, JWT::$key, true);
        $base64_url_signature = JWT::base64UrlEncode($signature);
    
        $is_signature_valid = ($base64_url_signature === $signature_provided);
        
        if ($is_token_expired || !$is_signature_valid) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    private static function base64UrlEncode($str){
        return rtrim(strtr(base64_encode($str), '+/', '-_'), '=');
    }
    
    public static function generarJWT($data){
        $headers = array('alg'=>'HS256','typ'=>'JWT');
        $payload = array(
            'data' => $data,
            'exp'=> (time() + 2 * 3600)
        );
        $headers_encoded = JWT::base64UrlEncode(json_encode($headers));
        $payload_encoded = JWT::base64UrlEncode(json_encode($payload));
        $signature = hash_hmac('SHA256', "$headers_encoded.$payload_encoded", JWT::$key, true);
        $signature_encoded = JWT::base64UrlEncode($signature);
        $jwt = "$headers_encoded.$payload_encoded.$signature_encoded";
        return $jwt;
    }
    
    public static function verificarJWT(){
        
        $headers = getallheaders();
        if (isset($headers['Authorization'])) {
            $token = str_replace("Bearer ", "", $headers['Authorization']);
            if(!JWT::validarJWT($token)){
                die(ArrestDB::Reply(ArrestDB::$HTTP[401]));
            }
    
            $payload = JWT::getDataJWT($token);
            if(isset($payload)){
                session_start();
                $_SESSION['usuario'] = $payload->data;
            }
    
        } else if(isset($_GET['Authorization'])){
            
            $token = str_replace("Bearer ", "", $_GET['Authorization']);
            if(JWT::validarJWT($token)){
                die(ArrestDB::Reply(ArrestDB::$HTTP[401]));
            }
    
            $payload = JWT::getDataJWT($token);
            if(isset($payload)){
                session_start();
                $_SESSION['usuario'] = $payload->data;
            }
    
        } else {
            die(ArrestDB::Reply(ArrestDB::$HTTP[401]));
        }
    }
    
    public static function hasPermission($table){
    
        if(in_array($table, JWT::$permissions['PUBLIC'])){
            return true;
        }
    
        $rol = $_SESSION['usuario']->rol;
        if($rol == 'ADMIN'){
            return true;
        }
    
        $roles = array_keys(JWT::$permissions);
    
        if(!in_array($rol, $roles)){
            return false;
        }
        
        if(!in_array($table, JWT::$permissions[$rol])){
            return false;
        }
    
        return true;
    }

}